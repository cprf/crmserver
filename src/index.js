/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
// make bluebird default Promise
Promise = require('bluebird'); // eslint-disable-line no-global-assign
const { port, env } = require('./config/vars');
const app = require('./config/express');
const mongoose = require('./config/mongoose');
const User = require('./api/models/user.model');
const tgService = require('./api/services/telegram.service');

const dayInterval = 60000 * 60 * 24;

// open mongoose connection
mongoose.connect();

// listen to requests
app.listen(port, () => console.info(`server started on port ${port} (${env})`));

const checkBotSubscription = async () => {
  const users = await User.find({ telegramId: { $ne: null } });

  for (const user of users) {
    const response = await tgService.sendAction(user.telegramId);

    if (!response.data.ok) {
      await User.update({ _id: user._id }, { $set: { telegramId: null } });
    }
  }
};

setInterval(() => {
  checkBotSubscription();
}, dayInterval);


/**
 * Exports express
 * @public
 */
module.exports = app;
