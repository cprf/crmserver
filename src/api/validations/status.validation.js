const Joi = require('joi');
const Status = require('../models/status.model');

module.exports = {

  // GET /v1/statuses
  listStatuses: {
    query: {
      type: Joi.string().valid(Status.types).required(),
    },
  },

  // POST /v1/statuses
  createStatus: {
    body: {
      type: Joi.string().valid(Status.types).required(),
    },
  },
};
