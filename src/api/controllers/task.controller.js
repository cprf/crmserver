const httpStatus = require('http-status');
const fs = require('fs');
const path = require('path');
const { botToken } = require('../../config/vars');
const { TelegramClient } = require('messaging-api-telegram');
const Task = require('../models/task.model');
const User = require('../models/user.model');
const Rating = require('../models/rating.model');
const Creative = require('../models/creative.model');
const Photo = require('../models/photo.model');

const client = TelegramClient.connect(botToken);

const STATUSES = {
  APPROVED: 1,
  NOTREVIEWED: 2,
};

async function updateRating(userId, taskId, points) {
  const record = { user_id: userId, task_id: taskId, points };
  const rating = new Rating(record);
  await rating.save();

  return User.update({ _id: userId }, { $inc: { points } });
}

/**
 * Create new task
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    const taskObject = JSON.parse(req.body.task);
    if (req.file) {
      taskObject.image = req.file.path;
    }
    const task = new Task(taskObject);
    const savedTask = await task.save();
    res.status(httpStatus.CREATED);
    res.json(savedTask);
  } catch (error) {
    next(error);
  }
};

/**
 * Get task list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const tasks = await Task.list();
    res.json(tasks);
  } catch (error) {
    next(error);
  }
};

/**
 * Get task by id
 * @public
 */
exports.get = async (req, res, next) => {
  try {
    const id = req.params.taskId;
    const task = await Task.get(id);
    const response = await Rating.aggregate([
      { $match: { task_id: task._id } },
      { $group: { _id: 0, points: { $sum: '$points' } } },
    ]);

    if (response.length) {
      task.points = response[0].points;
    }

    if (task.type.id === 'creative') {
      task.results = await Creative.listByTask(task._id);
    }

    res.json(task);
  } catch (error) {
    next(error);
  }
};

exports.getPhotos = async (req, res, next) => {
  try {
    const photos = await Photo.list(req.query);
    res.json(photos);
  } catch (error) {
    next(error);
  }
};

exports.updatePhoto = async (req, res, next) => {
  const photo = await Photo.get(req.params.id);
  photo.status = STATUSES.APPROVED;
  await photo.save();

  const points = photo.gps ? 10 : 5;
  await updateRating(photo.user, photo.task, points);

  res.status(httpStatus.NO_CONTENT).end();
};

exports.removePhoto = async (req, res, next) => {
  const photo = await Photo.get(req.params.id);

  if (photo) {
    const dirpath = path.resolve(__dirname, '../../..');
    const previewPath = `${dirpath}${photo.preview_path}`;
    const originalPath = `${dirpath}${photo.original_path}`;

    fs.unlinkSync(previewPath);
    // photos loaded as documents have the same original and preview paths
    if (previewPath !== originalPath) {
      fs.unlinkSync(originalPath);
    }

    await Photo.remove(req.params.id);
    res.status(httpStatus.NO_CONTENT).end();
  }
};

exports.remove = async (req, res, next) => {
  await Task.remove(req.params.taskId);
  res.status(httpStatus.NO_CONTENT).end();
};

exports.updateResultStatus = async (req, res, next) => {
  const {
    creativeId, userId, taskId, status, points,
  } = req.body;

  const user = await User.get(userId);

  let message = 'К сожалению, присланное вами выполнение задания не принято';

  await Creative.update({ _id: creativeId }, { $set: { status } });
  await User.update({ _id: userId }, { $set: { activeCreativeTask: null } });

  if (status === Creative.STATUSES.APPROVED) {
    await updateRating(userId, taskId, points);
    message = `Ваше выполнение задания принято, вам начислено ${points} баллов`;
  }

  await client.sendMessage(user.telegramId, message);

  res.status(httpStatus.NO_CONTENT).end();
};
