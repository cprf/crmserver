const httpStatus = require('http-status');
const mongoose = require('mongoose');
const mailService = require('../services/mail.service');
const Event = require('../models/event.model');
const User = require('../models/user.model');
const Rating = require('../models/rating.model');

/**
 * Create new event
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    const event = new Event(req.body);
    const savedEvent = await event.save();

    savedEvent.emailNotification.attachments = req.body.emailNotification.attachments;
    mailService.notify(savedEvent, req.user);

    res.status(httpStatus.CREATED).json(savedEvent);
  } catch (error) {
    next(error);
  }
};

/**
 * Get event list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    let events = await Event.list();
    const notAdmin = req.user.role !== 'city_admin' && req.user.role !== 'admin';
    if (notAdmin) {
      events = events.filter((event) => {
        if (event.type.value === 'innerEvents' || event.type.value === 'publicEvents') {
          const districtOffice = req.user.access || req.user.districtOffice;
          return event.organizer.districtOffice === districtOffice;
        }
        return event;
      });
    }
    res.json(events);
  } catch (error) {
    next(error);
  }
};

exports.get = async (req, res, next) => {
  try {
    const id = req.params.eventId;
    const eventId = mongoose.Types.ObjectId(id);
    const successMobileCalls = await User.count({ result: { $elemMatch: { callStatus: 'finished', eventId } } });
    const successSMS = await User.count({ result: { $elemMatch: { smsStatus: 'delivered', eventId } } });
    const successTgMessages = await User.count({ result: { $elemMatch: { tgStatus: 'delivered', eventId } } });

    // Get points statistic
    const stats = await Rating.find({ task_id: eventId }).populate('user_id', 'fio districtOffice');

    const event = await Event.get(id);
    event.successMobileCalls = successMobileCalls;
    event.successSMS = successSMS;
    event.successTgMessages = successTgMessages;
    event.stats = stats;

    res.json(event);
  } catch (error) {
    next(error);
  }
};

exports.callStatus = async (req, res, next) => {
  try {
    const apiCall = req.body.apiCalls[0];
    const call = apiCall.calls[0];
    const result = {
      callId: apiCall.id,
      callStatus: call.status,
      callHasBlocked: call.toBlock,
      user: call.phone,
    };

    await User.updateCallStatus(result);
    res.status(200).send('ok');
  } catch (error) {
    next(error);
  }
};

exports.smsStatus = async (req, res, next) => {
  const user = req.body.To.replace('+', '');
  const result = {
    user,
    smsStatus: req.body.SmsStatus,
    messageId: req.body.MessageSid,
  };

  try {
    await User.updateSmsStatus(result);
  } catch (error) {
    next(error);
  }
  res.status(200).send('ok');
};

exports.remove = async (req, res, next) => {
  await Event.remove(req.params.eventId);
  res.status(httpStatus.NO_CONTENT).end();
};
