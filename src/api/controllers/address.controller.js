const elasticsearch = require('elasticsearch');

const client = new elasticsearch.Client({
  host: 'localhost:9200',
});

/**
 * Get address by query
 * @public
 */

exports.query = async (req, res, next) => {
  try {
    const queryObject = {
      index: 'addresses',
      body: {
        query: {
          match_phrase_prefix: {
            address: {
              query: req.query.query,
              slop: 10,
              max_expansions: 50,
            },
          },
        },
        size: 10,
      },
    };

    const response = await client.search(queryObject);
    const addresses = response.hits.hits.map(val => (val._source));
    res.json(addresses);
  } catch (error) {
    next(error);
  }
};
