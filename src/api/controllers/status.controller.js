const httpStatus = require('http-status');
const Status = require('../models/status.model');


/**
 * Create new status
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    const status = new Status(req.body);
    const savedStatus = await status.save();
    res.status(httpStatus.CREATED);
    res.json(savedStatus);
  } catch (error) {
    next(error);
  }
};

/**
 * Get status list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const statuses = await Status.list();
    res.json(statuses);
  } catch (error) {
    next(error);
  }
};
