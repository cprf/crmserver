const { TelegramClient } = require('messaging-api-telegram');
const { botToken } = require('../../config/vars');
const Rating = require('../models/rating.model');
const Task = require('../models/task.model');
const User = require('../models/user.model');

const client = TelegramClient.connect(botToken);

/**
 * Handle link
 * @public
 */
exports.handleLink = async (req, res, next) => {
  try {
    const { link, userId } = req.query;

    if (!userId) {
      return res.redirect(link);
    }
    const ip = req.headers['x-forwarded-for'].split(',').pop();
    const task = await Task.findByLink(link);

    const checkIp = await Rating.findOne({ user_id: userId, task_id: task._id, ip });
    if (checkIp) {
      return res.redirect(link);
    }
    // check if user reached limit of 300 points for this link
    const totalPoints = await Rating.count({ user_id: userId, task_id: task._id });
    if (totalPoints < 300) {
      // update points in users collection
      await User.update({ _id: userId }, { $inc: { points: 1 } });
      // add record to the rating collection
      const record = {
        user_id: userId,
        task_id: task._id,
        points: 1,
        ip,
      };
      const rating = new Rating(record);
      await rating.save();
    }

    const user = await User.findById(userId);
    const hasAchievement = user.achievements.find(el => el.id === 'hot_news');

    if (totalPoints === 300 && !hasAchievement) {
      const achievement = {
        id: 'hot_news',
        name: 'Вот это новость! Заработайте 300 баллов за просмотр новости',
        points: 150,
      };

      await User.update(
        { _id: userId },
        { $addToSet: { achievements: achievement }, $inc: { points: 150 } },
      );

      const bonus = {
        user_id: userId,
        points: 150,
        type: 'bonus',
      };

      const bonusRating = new Rating(bonus);
      await bonusRating.save();

      if (user.telegramId) {
        const message = `Ура! Вами получено достижение: ${achievement.name}`;
        await client.sendMessage(user.telegramId, message);
      }
    }

    return res.redirect(link);
  } catch (error) {
    return next(error);
  }
};

exports.getRating = async (req, res) => {
  const response = await Rating.calculateRating(req.query);
  res.json(response);
};
