const moment = require('moment-timezone');
const carbone = require('carbone');
const httpStatus = require('http-status');
const { omit, isEmpty } = require('lodash');
const diff = require('object-diff');
const xlsxj = require('xlsx-to-json');
const fs = require('fs');
const path = require('path');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const User = require('../models/user.model');
const Notification = require('../models/notification.model');
const Rating = require('../models/rating.model');
const UserHistory = require('../models/userHistory.model');
const { handler: errorHandler } = require('../middlewares/error');
const mailService = require('../services/mail.service');
const telegramService = require('../services/telegram.service');
const { baseUrl, dbName } = require('../../config/vars');

/**
 * Load user and append to req.
 * @public
 */
exports.load = async (req, res, next, id) => {
  try {
    const user = await User.get(id);
    req.locals = { user };
    return next();
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

/**
 * Get user
 * @public
 */
exports.get = (req, res) => res.json(req.locals.user.transform());

/**
 * Get logged in user info
 * @public
 */
exports.loggedIn = (req, res) => res.json(req.user.transform());

/**
 * Create new user
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    let user = {};
    // check if user has already registered through telegram
    const tUser = await User.findOne({
      mobilePhone: req.body.mobilePhone,
      districtOffice: 'Телеграм',
    });
    if (tUser) {
      user = Object.assign(tUser, req.body);
    } else {
      user = new User(req.body);
    }

    user.addedBy = { id: req.user._id, fio: req.user.fio };
    const savedUser = await user.save();
    res.status(httpStatus.CREATED);
    res.json(savedUser.transform());
  } catch (error) {
    next(User.checkDuplicateEmail(error));
  }
};

/**
 * Create new user from another site
 * @public
 */
exports.external = async (req, res, next) => {
  try {
    let user;
    // check if user has already registered through telegram
    const tUser = await User.findOne({
      mobilePhone: req.body.mobilePhone,
      districtOffice: 'Телеграм',
    });
    if (tUser) {
      user = Object.assign(tUser, req.body);
    } else {
      user = new User(req.body);
    }

    user.addedBy = { id: 'external', fio: 'Через форму' };
    const savedUser = await user.save();
    sendNotification(savedUser);
    res.status(httpStatus.CREATED);
    res.json(savedUser.transform());
  } catch (error) {
    next(User.checkDuplicateEmail(error));
  }
};

/**
 * Replace existing user
 * @public
 */
exports.replace = async (req, res, next) => {
  try {
    const { user } = req.locals;
    const newUser = new User(req.body);
    const ommitRole = user.role !== 'admin' ? 'role' : '';
    const newUserObject = omit(newUser.toObject(), '_id', ommitRole);

    await user.update(newUserObject, { override: true, upsert: true });
    const savedUser = await User.findById(user._id);

    res.json(savedUser.transform());
  } catch (error) {
    next(User.checkDuplicateEmail(error));
  }
};

/**
 * Update existing user
 * @public
 */
exports.update = async (req, res, next) => {
  const user = await User.get(req.body._id);
  const d = diff(user.address, req.body.address);

  const updatedUser = Object.assign(user, req.body);

  if (!isEmpty(d)) {
    updatedUser.addressHasChanged = true;
  }
  updatedUser.modifiedBy = req.user.fio;

  updatedUser
    .save()
    .then(savedUser => res.json(savedUser.transform()))
    .catch(e => next(User.checkDuplicateEmail(e)));
};

/**
 * Get user list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    let users = await User.list(req.query, req.user);
    users = users.filter(user => user.role !== 'admin');
    const transformedUsers = users.map(user => user.transform());
    res.json(transformedUsers);
  } catch (error) {
    next(error);
  }
};

/**
 * Delete user
 * @public
 */
exports.remove = (req, res, next) => {
  const { user } = req.locals;

  user
    .remove()
    .then(() => res.status(httpStatus.NO_CONTENT).end())
    .catch(e => next(e));
};

/**
 * User edit history
 * @public
 */
exports.history = async (req, res, next) => {
  try {
    const id = req.query.userId ? req.query.userId : req.user._id;
    const response = await UserHistory.get(id);
    res.json(response);
  } catch (error) {
    next(error);
  }
};

exports.requests = async (req, res, next) => {
  try {
    const params = {};
    params.addedBy = 'external';
    params.primaryOffice = null;
    const requests = await User.list(params, req.user);
    res.json(requests);
  } catch (error) {
    next(error);
  }
};

/**
 * Batch adding
 * @public
 */
exports.batch = (req, res, next) => {
  async function handler(error, result) {
    if (error) {
      next(error);
    }

    try {
      const supporters = [];
      // eslint-disable-next-line
      for (let supporter of result) {
        // eslint-disable-next-line
        const tUser = await User.findOne({
          mobilePhone: supporter.mobilePhone,
          districtOffice: 'Телеграм',
        });

        const street = supporter.address;
        supporter.address = { street, flat: supporter.flat };
        supporter.addedBy = { id: req.user._id, fio: req.user.fio };
        supporter.fio = getFullFio(supporter);
        supporter.fullAddress = getFullAddress(supporter);
        delete supporter.flat;

        supporter.bdate = supporter.bdate
          .replace(/\./g, '-')
          .split('-')
          .reverse()
          .join('-');
        if (supporter.partyStatuses.length) {
          supporter.partyStatuses = supporter.partyStatuses.trim().split(',');
        }
        if (supporter.publicStatuses.length) {
          supporter.publicStatuses = supporter.publicStatuses.trim().split(',');
        }
        if (supporter.alertOptions.length) {
          supporter.alertOptions = supporter.alertOptions.trim().split(',');
        }

        if (tUser) {
          supporter = Object.assign(tUser, supporter);
          // eslint-disable-next-line
          await supporter.save();
        } else {
          supporters.push(supporter);
        }
      }
      await User.insertMany(supporters);
    } catch (e) {
      next(e);
    }

    fs.unlink(req.file.path, () => { });
    res.status(httpStatus.CREATED).end();
  }

  try {
    xlsxj(
      {
        input: req.file.path,
        output: null,
      },
      handler,
    );
  } catch (error) {
    next(error);
  }
};

const fields =
  '_id,fio,bdate,mobilePhone,phone,fullAddress,address.flat,hasTelegram,district,districtOffice,partyStatuses,publicStatuses,comment,primaryOffice,email,telegramId,points,addedBy,createdAt,result';

/**
 * Creates dump of users collection
 * @public
 */
exports.dump = async (req, res, next) => {
  const dirpath = path.resolve(__dirname, '../../../uploads/');
  const filePath = `${baseUrl}/uploads/users.csv`;
  const command = `mongoexport -d ${dbName} -c users --type=csv --fields ${fields} --out ${dirpath}/users.csv`;

  await exec(command);
  res.json({ path: filePath });
};

exports.dumphistory = async (req, res, next) => {
  const dirpath = path.resolve(__dirname, '../../../uploads/');
  const filePath = `${baseUrl}/uploads/usershistory.csv`;
  const command = `mongoexport -d ${dbName} -c userhistories --type=csv --fields ${fields} --out ${dirpath}/users.csv`;

  await exec(command);
  res.json({ path: filePath });
};

/**
 * Get statistics
 * @public
 */
function getPoinstByType(type, districtOffice, rating) {
  const filteredRating = rating
    .filter(r => r.user_id && r.task_id)
    .filter(r => r.user_id.districtOffice === districtOffice && r.task_id.type.id === type);

  return filteredRating.reduce((acc, curr) => {
    const p = curr.points || 0;
    // eslint-disable-next-line
    return (acc += p);
  }, 0);
}

exports.statistics = async (req, res, next) => {
  const params = req.user.access || null;
  let statistics = await User.getStats(params);

  statistics = statistics
    .filter(o => o._id)
    .map((o) => {
      const statObject = Object.assign({}, o);
      const unprocessedRequests = o.unprocessedRequests.filter(r => r);
      const admins = o.admins.filter(a => a);

      if (unprocessedRequests.length) {
        const latestRequest = new Date(Math.min.apply(
          null,
          unprocessedRequests.map(e => new Date(e)),
        ));
        statObject.latestRequest = latestRequest;
      }
      if (admins.length) {
        const lastSeenAdmin = new Date(Math.max.apply(
          null,
          admins.map(a => (a.lastSeen ? new Date(a.lastSeen) : 0)),
        ));
        statObject.lastSeenAdmin = lastSeenAdmin;
      }

      delete statObject.unprocessedRequests;
      delete statObject.admins;

      return statObject;
    });

  const rating = await Rating.find()
    .populate({
      path: 'user_id',
      select: 'districtOffice',
    })
    .populate({
      path: 'task_id',
      select: 'type.id',
    });
  // eslint-disable-next-line
  for (let stat of statistics) {
    const creativePoints = getPoinstByType('creative', stat._id, rating);
    const spreadPoints = getPoinstByType('internet_spread', stat._id, rating);
    const advertisingPoints = getPoinstByType('agit_advertising', stat._id, rating);

    stat.creativePoints = creativePoints;
    stat.spreadPoints = spreadPoints;
    stat.advertisingPoints = advertisingPoints;

    if (stat._id === 'Телеграм') {
      stat.hasEmail = 0;
      stat.hasAddress = 0;
    }
  }

  res.json(statistics);
};

/**
 * Get list of creators(users who can add another users to the system)
 * @public
 */
exports.creators = async (req, res, next) => {
  let creators = await User.getCreators();
  creators = creators.reduce((acc, user) => {
    const isExists = acc.find(u => String(u.id) === String(user.id));
    return isExists ? acc : [...acc, user];
  }, []);

  creators = creators.filter(u => u.fio !== '' || !u.id);
  res.json(creators);
};

/**
 * Check if phone is registered in Telegram
 * @public
 */
exports.checkTelegram = async (req, res, next) => {
  const { phone_registered: phoneRegistered } = await telegramService.checkPhone(req.query.phone);
  res.json({ isRegistered: phoneRegistered });
};

const getFullFio = supporter => `${supporter.lName} ${supporter.fName} ${supporter.mName}`.trim();

const getFullAddress = (supporter) => {
  let address = supporter.address.street;
  if (supporter.flat.length) {
    address += `, ${supporter.flat}`;
  }
  return address;
};

const sendNotification = async (doc) => {
  const response = await Notification.findByOffice({ districtOffice: doc.districtOffice });

  if (response) {
    const smsText = 'У вас новая заявка от сторонника КПРФ с регионального сайта';
    const emailText = `С сайта кпрф поступила новая заявка от сторонника КПРФ, относящегося
      к вашему местному отделению. ФИО Сторонника - ${doc.fio}. Полная информация доступна в системе Аврора`;

    const params = {
      email: response.email,
      phone: response.phone,
      smsText,
      emailText,
    };

    mailService.nofityExternal(params);
  }
};

exports.report = async (req, res) => {
  const districtOffice = req.body.districtOffice || 'г. Москва';
  const query = {};

  if (req.body.districtOffice) {
    query.districtOffice = req.body.districtOffice;
  } else {
    query.$and = [
      { districtOffice: { $ne: 'Удаленные' } },
      { districtOffice: { $ne: 'Телеграм' } },
    ];
  }

  const membersQuery = { ...query, partyStatuses: 'Член КПРФ' };

  const age30Callback = (member) => {
    const years = moment().diff(moment(member.bdate), 'years');
    return years < 30;
  };

  const age40Callback = (member) => {
    const years = moment().diff(moment(member.bdate), 'years');
    return years >= 30 && years < 40;
  };

  const age50Callback = (member) => {
    const years = moment().diff(moment(member.bdate), 'years');
    return years >= 40 && years < 50;
  };

  const ageUnder60Callback = (member) => {
    const years = moment().diff(moment(member.bdate), 'years');
    return years >= 50 && years < 60;
  };

  const age60Callback = (member) => {
    const years = moment().diff(moment(member.bdate), 'years');
    return years >= 60;
  };

  // Количество первичных отделений в МО
  let primaryOfficeCount = await User.distinct('primaryOffice', query);
  primaryOfficeCount = primaryOfficeCount.filter(Boolean).length;
  // Количество человек со статусом Член КПРФ
  const members = await User.find(membersQuery);

  const totalAge = members.reduce((acc, curr) => {
    const years = moment().diff(moment(curr.bdate), 'years');
    return years > 0 ? acc + years : acc;
  }, 0);

  const status7 = members.filter(m => m.partyStatuses.includes('Член парткома'));
  const status7Under30 = status7.filter(age30Callback).length;
  const status7Greater60 = status7.filter(age60Callback).length;

  const status8 = members.filter(m => m.partyStatuses.includes('Кандидат в члены парткома'));
  const status8Under30 = status8.filter(age30Callback).length;
  const status8Greater60 = status8.filter(age60Callback).length;

  const status9 = members.filter(m => m.partyStatuses.includes('Член бюро парткома'));
  const status9Under30 = status9.filter(age30Callback).length;
  const status9Greater60 = status9.filter(age60Callback).length;

  const status10 = members.filter(m => m.partyStatuses.includes('секретарь РК КПРФ'));
  const status10Under30 = status10.filter(age30Callback).length;
  const status10Greater60 = status10.filter(age60Callback).length;

  const status14 = members.filter(m => m.publicStatuses.includes('член ЛКСМ'));

  const contributers = members
    .filter(m =>
      m.contributionDateTill
      && moment(req.body.date).isBefore(moment(m.contributionDateTill)))
    .length;

  const initiated = members
    .filter(m =>
      m.initiationDate
      && moment(req.body.date).isBefore(moment(m.initiationDate)));

  const initiatedUnder30 = initiated.filter(age30Callback);
  const withEmail = members.filter(m => m.email && m.email.length).length;

  const data = {
    districtOffice,
    startDate: req.body.date,
    endDate: new Date().toLocaleDateString(),
    primaryOfficeCount,
    membersCount: members.length,
    membersByAge: {
      30: members.filter(age30Callback).length,
      40: members.filter(age40Callback).length,
      50: members.filter(age50Callback).length,
      60: members.filter(ageUnder60Callback).length,
      greater60: members.filter(age60Callback).length,
      notSpecified: members.filter(m => !m.bdate).length,
    },
    avgAge: Math.floor(totalAge / members.length),
    byStatus: {
      female: members.filter(m => m.gender === 'female').length,
      worker: members.filter(m => m.publicStatuses.includes('Рабочий')).length,
      agrarian: members.filter(m => m.publicStatuses.includes('Аграрий')).length,
      servant: members.filter(m => m.publicStatuses.includes('Служащий')).length,
      itr: members.filter(m => m.publicStatuses.includes('Работник ИТР')).length,
      creative: members.filter(m => m.publicStatuses.includes('Представитель творческой интеллигенции')).length,
      head: members.filter(m => m.publicStatuses.includes('Руководитель предприятия')).length,
      entrepreneur: members.filter(m => m.publicStatuses.includes('Предприниматель')).length,
      student: members.filter(m => m.publicStatuses.includes('Учашийся')).length,
      retiree: members.filter(m => m.publicStatuses.includes('Пенсионер')).length,
      unemployed: members.filter(m => m.publicStatuses.includes('Безработный')).length,
    },
    contributers,
    initiated: {
      total: initiated.length,
      under30: initiatedUnder30.length,
    },
    status7: {
      total: status7.length,
      under30: status7Under30,
      greater60: status7Greater60,
    },
    status8: {
      total: status8.length,
      under30: status8Under30,
      greater60: status8Greater60,
    },
    status9: {
      total: status9.length,
      under30: status9Under30,
      greater60: status9Greater60,
    },
    status10: {
      total: status10.length,
      under30: status10Under30,
      greater60: status10Greater60,
    },
    status14: status14.length,
    withEmail,
  };

  carbone.render('reports/stat_template.docx', data, (err, result) => {
    if (err) {
      return console.log(err);
    }
    // write the result
    const reportPath = 'reports/result.docx';
    fs.writeFileSync(reportPath, result);
    return res.download(reportPath);
  });
};
