const httpStatus = require('http-status');
const Notification = require('../models/notification.model');

exports.list = async (req, res, next) => {
  try {
    const notifications = await Notification.list();
    res.json(notifications);
  } catch (error) {
    next(error);
  }
};

exports.create = async (req, res, next) => {
  try {
    const notification = new Notification(req.body);
    const savedNotification = await notification.save();
    res.status(httpStatus.CREATED);
    res.json(savedNotification);
  } catch (error) {
    next(error);
  }
};

exports.update = async (req, res, next) => {
  const notification = await Notification.get(req.body._id);
  const updatedNotification = Object.assign(notification, req.body);

  updatedNotification
    .save()
    .then(savedNotification => res.json(savedNotification))
    .catch(e => next(e));
};
