const mongoose = require('mongoose');

/**
 * Address Schema
 * @private
 */
const addressSchema = new mongoose.Schema({
  address: String,
  district: String,
  kprf: String,
});

addressSchema.statics = {
  list(params) {
    const q = new RegExp(params.query, 'i');
    return this.find({ address: q }).limit(10).exec();
  },
};

/**
 * @typedef Address
 */
const Address = mongoose.model('Address', addressSchema);
module.exports = Address;
