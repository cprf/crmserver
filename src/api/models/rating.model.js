const mongoose = require('mongoose');
const User = require('./user.model');
const { TelegramClient } = require('messaging-api-telegram');
const { botToken } = require('../../config/vars');

const client = TelegramClient.connect(botToken);

/**
 * Rating Schema
 * @private
 */
const ratingSchema = new mongoose.Schema(
  {
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    task_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Task' },
    points: Number,
    ip: String,
    type: String,
  },
  { timestamps: true },
);

const eventsAcheivements = [
  {
    id: 'alive',
    name: 'Спасибо, что живой. Впервые зарегистрируйтесь на мероприятии',
    points: 50,
  },
  {
    id: 'frequent_guest',
    name: 'Частый гость. Зарегистрируйтесь на 3 мероприятиях',
    points: 200,
  },
  {
    id: 'customer',
    name: 'Завсегдатай. Зарегистрируйтесь на 10 мероприятиях',
    points: 500,
  },
];

ratingSchema.post('save', async (doc, next) => {
  try {
    if (doc.type === 'event') {
      const registeredEvents = await Rating.count({
        user_id: doc.user_id,
        type: 'event',
      });

      const user = await User.findById(doc.user_id);

      if (registeredEvents === 1) {
        await User.update(
          { _id: doc.user_id },
          {
            $addToSet: { achievements: eventsAcheivements[0] },
            $inc: { points: eventsAcheivements[0].points },
          },
        );

        const bonus = {
          user_id: doc.user_id,
          points: eventsAcheivements[0].points,
          type: 'bonus',
        };

        const bonusRating = new Rating(bonus);
        await bonusRating.save();

        const message = `Ура! Вами получено достижение: ${eventsAcheivements[0].name}`;
        await client.sendMessage(user.telegramId, message);
      } else if (registeredEvents === 3) {
        await User.update(
          { _id: doc.user_id },
          {
            $addToSet: { achievements: eventsAcheivements[1] },
            $inc: { points: eventsAcheivements[1].points },
          },
        );

        const bonus = {
          user_id: doc.user_id,
          points: eventsAcheivements[1].points,
          type: 'bonus',
        };

        const bonusRating = new Rating(bonus);
        await bonusRating.save();

        const message = `Ура! Вами получено достижение: ${eventsAcheivements[1].name}`;
        await client.sendMessage(user.telegramId, message);
      } else if (registeredEvents === 10) {
        await User.update(
          { _id: doc.user_id },
          {
            $addToSet: { achievements: eventsAcheivements[2] },
            $inc: { points: eventsAcheivements[2].points },
          },
        );

        const bonus = {
          user_id: doc.user_id,
          points: eventsAcheivements[2].points,
          type: 'bonus',
        };

        const bonusRating = new Rating(bonus);
        await bonusRating.save();

        const message = `Ура! Вами получено достижение: ${eventsAcheivements[2].name}`;
        await client.sendMessage(user.telegramId, message);
      }
    }

    return next();
  } catch (error) {
    return next(error);
  }
});

ratingSchema.statics = {
  list(query) {
    return this.find(query).exec();
  },
  calculateRating(params) {
    const from = new Date(params.from);
    const to = new Date(params.to);

    return this.aggregate([
      {
        $match: { createdAt: { $gte: from, $lte: to } },
      },
      {
        $group: { _id: '$user_id', rating: { $sum: '$points' } },
      },
      {
        $lookup: {
          from: 'users',
          let: { userId: '$_id' },
          pipeline: [
            {
              $match: {
                $expr: { $eq: ['$_id', '$$userId'] },
              },
            },
            {
              $project: {
                _id: 0, mName: 1, fName: 1, lName: 1,
              },
            },
          ],
          as: 'user',
        },
      },
      {
        $project: {
          user: {
            $arrayElemAt: ['$user', 0],
          },
          rating: 1,
        },
      },
      {
        $sort: { rating: -1 },
      },
    ]).exec();
  },
};

/**
 * @typedef Rating
 */
const Rating = mongoose.model('Rating', ratingSchema);
module.exports = Rating;
