const mongoose = require('mongoose');
const APIError = require('../utils/APIError');
const httpStatus = require('http-status');
/**
 * Task Schema
 * @private
 */
const taskSchema = new mongoose.Schema({
  name: String,
  type: Object,
  spread: {
    link: String,
    comment: String,
    dateTo: Date,
    dateFrom: Date,
  },
  advertising: {
    description: String,
    dateTo: Date,
    dateFrom: Date,
  },
  creative: {
    points: Number,
    description: String,
  },
  image: String,
}, {
  timestamps: true,
});

taskSchema.statics = {
  list() {
    return this.find().lean().exec();
  },
  /**
   * Get task
   *
   * @param {ObjectId} id - The objectId of task.
   * @returns {Promise<task, APIError>}
   */
  async get(id) {
    try {
      let task;

      if (mongoose.Types.ObjectId.isValid(id)) {
        task = await this.findById(id).lean().exec();
      }
      if (task) {
        return task;
      }

      throw new APIError({
        message: 'Task does not exist',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },
  findByLink(link) {
    return this.findOne({ 'spread.link': link }).exec();
  },
  async remove(id) {
    try {
      const task = await this.findByIdAndRemove(id).exec();
      return task;
    } catch (error) {
      throw error;
    }
  },
};

/**
 * @typedef Task
 */
const Task = mongoose.model('Task', taskSchema);
module.exports = Task;

