const mongoose = require('mongoose');
const httpStatus = require('http-status');
const axios = require('axios');
const { TelegramClient } = require('messaging-api-telegram');
const { omitBy, omit, pullAll } = require('lodash');
const bcrypt = require('bcryptjs');
const moment = require('moment-timezone');
const jwt = require('jwt-simple');
const APIError = require('../utils/APIError');
const {
  env, jwtSecret, jwtExpirationInterval, city, botToken,
} = require('../../config/vars');
const UserHistory = require('../models/userHistory.model');

const client = TelegramClient.connect(botToken);

/**
 * User Roles
 */
const roles = [
  'user',
  'admin',
  'superadmin',
  'district_admin',
  'local_admin',
  'city_admin',
  'status_admin',
];

const arrayType = {
  type: [String],
  set: v => (v.length ? v : []),
};

function getLevel(points) {
  let level = 1;

  if (points >= 10 && points < 100) level = 2;
  if (points >= 100 && points < 500) level = 3;
  if (points >= 500 && points < 1000) level = 4;
  if (points >= 1000 && points < 2000) level = 5;
  if (points >= 2000 && points < 4000) level = 6;
  if (points >= 4000 && points < 8000) level = 7;
  if (points >= 8000 && points < 16000) level = 8;
  if (points >= 16000 && points < 32000) level = 9;
  if (points >= 32000 && points < 64000) level = 10;
  if (points >= 64000 && points < 128000) level = 11;
  if (points >= 128000) level = 12;

  return level;
}

/**
 * User Schema
 * @private
 */
const userSchema = new mongoose.Schema(
  {
    fName: String,
    mName: String,
    lName: String,
    fio: String,
    bdate: {
      type: Date,
      default: null,
      set: v => (v.length ? new Date(v) : null),
    },
    mobilePhone: {
      type: String,
      maxlength: 11,
      index: {
        unique: true,
        partialFilterExpression: { mobilePhone: { $type: 'string' } },
      },
      set: v => (v.length ? v : null),
    },
    phone: {
      type: String,
      maxlength: 11,
      default: '',
    },
    address: {
      street: String,
      building: String,
      flat: String,
    },
    fullAddress: {
      type: String,
      trim: true,
    },
    hasTelegram: Boolean,
    district: String,
    districtOffice: String,
    partyStatuses: arrayType,
    publicStatuses: arrayType,
    alertOptions: arrayType,
    comment: String,
    primaryOffice: Number,
    email: {
      type: String,
      match: /^\S+@\S+\.\S+$/,
      trim: true,
      lowercase: true,
      index: {
        unique: true,
        partialFilterExpression: { email: { $type: 'string' } },
      },
      set: v => (v.length ? v : null),
    },
    password: {
      type: String,
      minlength: 6,
      maxlength: 128,
    },
    role: {
      type: String,
      default: 'user',
    },
    access: String,
    addedBy: Object,
    updatedBy: String,
    changedAddress: {
      type: Boolean,
      default: false,
    },
    result: [],
    telegramId: Number,
    points: {
      type: Number,
      default: 0,
    },
    lastSeen: Date,
    level: Number,
    activeTask: mongoose.Schema.Types.ObjectId,
    activeCreativeTask: mongoose.Schema.Types.ObjectId,
    coords: Array,
    achievements: Array,
    gender: String,
    partyTicket: Number,
    initiationDate: Date,
    contributionDateTill: Date,
  },
  {
    timestamps: true,
    usePushEach: true,
  },
);

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
userSchema.pre('save', async function save(next) {
  try {
    if (!this.isModified('password')) return next();

    const rounds = env === 'test' ? 1 : 10;

    const hash = await bcrypt.hash(this.password, rounds);
    this.password = hash;

    return next();
  } catch (error) {
    return next(error);
  }
});

const geocodeUrl = 'https://geocode-maps.yandex.ru/1.x/';
const geoApiKey = '9fd095cf-31a0-4939-abf5-eeb3397a934e';
// Set coords
userSchema.pre('save', async function save(next) {
  try {
    if (this.isNew || this.changedAddress) {
      const geocode = `${city}, ${this.address.street}`;
      const params = { format: 'json', geocode, apikey: geoApiKey };
      const response = await axios.get(geocodeUrl, { params });
      const { pos } = response.data.response.GeoObjectCollection.featureMember[0].GeoObject.Point;
      this.coords = pos.split(' ');
    }

    return next();
  } catch (error) {
    return next(error);
  }
});

// History update
userSchema.pre('save', async function save(next) {
  try {
    if (!this.isNew) {
      // Do not update history if this is last seen update
      if (this.modifiedPaths().includes('lastSeen')) {
        return next();
      }
      const ignoredFields = ['fio', 'updatedBy', 'updatedAt', 'result', 'changedAddress', 'coords'];
      if (!this.changedAddress) {
        ignoredFields.push('address');
      }
      const fields = pullAll(this.modifiedPaths(), ignoredFields);

      if (fields.length) {
        const history = {
          userId: this._id,
          fields,
          updatedBy: this.updatedBy,
          updatedAt: this.updatedAt,
        };

        await UserHistory.create(history);
      }
      next();
    }
    return next();
  } catch (error) {
    return next(error);
  }
});

const levels = {
  2: 10,
  3: 100,
  4: 500,
  5: 1000,
  6: 2000,
  7: 4000,
  8: 8000,
  9: 16000,
  10: 32000,
  11: 64000,
  12: 128000,
};

userSchema.pre('save', async function save(next) {
  try {
    if (!this.isNew && this.modifiedPaths().includes('points') && this.telegramId) {
      const lvl = (this.level + 1).toString();

      if (this.points >= levels[lvl]) {
        const newLevel = getLevel(this.points);
        const message = `Ура! Вами получено достижение: ${newLevel} уровень`;
        this.level = newLevel;
        await client.sendMessage(this.telegramId, message);
      }

      next();
    }
    return next();
  } catch (error) {
    return next(error);
  }
});

userSchema.virtual('modifiedBy').set(function a(user) {
  if (!this.isNew) {
    this.updatedBy = user;
  }
});

userSchema.virtual('addressHasChanged').set(function b(value) {
  if (!this.isNew) {
    this.changedAddress = value;
  }
});

/**
 * Methods
 */
userSchema.method({
  transform() {
    return omit(this, 'password');
  },

  token() {
    const playload = {
      exp: moment()
        .add(jwtExpirationInterval, 'minutes')
        .unix(),
      iat: moment().unix(),
      sub: this._id,
    };
    return jwt.encode(playload, jwtSecret);
  },

  async passwordMatches(password) {
    return bcrypt.compare(password, this.password);
  },
});

/**
 * Statics
 */
userSchema.statics = {
  roles,

  /**
   * Get user
   *
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  async get(id) {
    try {
      let user;

      if (mongoose.Types.ObjectId.isValid(id)) {
        user = await this.findById(id).exec();
      }
      if (user) {
        return user;
      }

      throw new APIError({
        message: 'User does not exist',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },

  /**
   * Get creators
   *
   * @returns {Promise<User, APIError>}
   */
  getCreators() {
    return this.aggregate([
      { $group: { _id: '$addedBy' } },
      { $project: { _id: 0, id: '$_id.id', fio: '$_id.fio' } },
    ]).exec();
  },

  getStats(districtOffice) {
    const match = {};
    if (districtOffice) {
      match.districtOffice = districtOffice;
    }

    const unprocessedRequest = {
      $and: [{ $eq: ['$primaryOffice', null] }, { $eq: ['$addedBy.id', 'external'] }],
    };

    return this.aggregate([
      { $match: match },
      {
        $project: {
          phone: 1,
          mobilePhone: 1,
          email: 1,
          districtOffice: 1,
          points: 1,
          addedBy: 1,
          fio: 1,
          createdAt: 1,
          lastSeen: 1,
          primaryOffice: { $ifNull: ['$primaryOffice', null] },
          addedByAdmin: { $cond: [{ $eq: ['$addedBy.fio', 'Администратор'] }, 1, 0] },
          addedFromForm: { $cond: [{ $eq: ['$addedBy.id', 'external'] }, 1, 0] },
          isDistrictAdmin: { $eq: ['$role', 'district_admin'] },
          hasPhone: { $cond: [{ $eq: ['$phone', ''] }, 0, 1] },
          hasMobile: { $cond: [{ $eq: ['$mobilePhone', null] }, 0, 1] },
          hasEmail: { $cond: [{ $eq: ['$email', null] }, 0, 1] },
          hasAddress: { $cond: [{ $eq: ['$fullAddress', ''] }, 0, 1] },
        },
      },
      {
        $group: {
          _id: '$districtOffice',
          total: { $sum: 1 },
          totalPoints: { $sum: '$points' },
          addedByAdmin: { $sum: '$addedByAdmin' },
          addedFromForm: { $sum: '$addedFromForm' },
          hasPhone: { $sum: '$hasPhone' },
          hasMobile: { $sum: '$hasMobile' },
          hasEmail: { $sum: '$hasEmail' },
          hasAddress: { $sum: '$hasAddress' },
          totalUnprocessedRequests: {
            $sum: {
              $cond: [unprocessedRequest, 1, 0],
            },
          },
          unprocessedRequests: {
            $addToSet: {
              $cond: [unprocessedRequest, '$createdAt', null],
            },
          },
          admins: {
            $addToSet: {
              $cond: {
                if: '$isDistrictAdmin',
                then: { fio: '$fio', lastSeen: '$lastSeen' },
                else: null,
              },
            },
          },
        },
      },
    ]).exec();
  },

  /**
   * Find user by email and tries to generate a JWT token
   *
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  async findAndGenerateToken(options) {
    const { email, password, refreshObject } = options;
    if (!email) throw new APIError({ message: 'An email is required to generate a token' });

    const user = await this.findOne({ email }).exec();
    const err = {
      status: httpStatus.UNAUTHORIZED,
      isPublic: true,
    };
    if (password) {
      if (user && (await user.passwordMatches(password))) {
        return { user, accessToken: user.token() };
      }
      err.message = 'Incorrect email or password';
    } else if (refreshObject && refreshObject.userEmail === email) {
      return { user, accessToken: user.token() };
    } else {
      err.message = 'Incorrect email or refreshToken';
    }
    throw new APIError(err);
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   *
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list(params, user) {
    if (params.eventResults) {
      // eslint-disable-next-line no-param-reassign
      params.eventResults = JSON.parse(params.eventResults);
    }

    // eslint-disable-next-line no-param-reassign
    params = omitBy(params, p => p === '' || p === 'false');
    const query = {};

    if (params.hasPhone) {
      query.phone = { $ne: '' };
    }

    if (params.hasMobilePhone) {
      query.mobilePhone = { $ne: null };
    }

    if (params.hasEmail) {
      query.email = { $ne: null };
    }

    if (params.hasTelegram) {
      query.telegramId = { $ne: null };
    }

    if (params.email) {
      query.email = params.email;
    }

    if (params.gender) {
      query.gender = params.gender;
    }

    if (params.partyTicket) {
      query.partyTicket = params.partyTicket;
    }

    if (params.alertOptions) {
      query.alertOptions = { $in: params.alertOptions };
    }

    if (params.districtOffice) {
      query.districtOffice = params.districtOffice;
    }

    if (params.district) {
      query.district = params.district;
    }

    if (params.phone) {
      query.phone = params.phone;
    }

    if (params.mobilePhone) {
      query.mobilePhone = params.mobilePhone;
    }

    if (params.fio || params.address) {
      const text = params.fio || params.address;
      query.$text = { $search: `'${text}'` };
    }

    if (params.primaryOffice) {
      if (Array.isArray(params.primaryOffice)) {
        query.$or = params.primaryOffice.map(po => ({
          primaryOffice: Number(po),
        }));
      } else {
        query.primaryOffice = Number(params.primaryOffice);
      }
    }

    if (params.primaryOffice === null) {
      query.primaryOffice = null;
    }

    if (params.bdateFrom && params.bdateTo) {
      const start = new Date(params.bdateFrom);
      const end = new Date(params.bdateTo);
      query.bdate = { $gte: start, $lte: end };
    }

    if (params.initiationDate) {
      const date = new Date(params.initiationDate);
      const y = date.getFullYear();
      const m = date.getMonth();

      const firstDay = new Date(y, m, 1);
      const lastDay = new Date(y, m + 1, 0);
      query.initiationDate = { $gte: firstDay, $lte: lastDay };
    }

    if (params.contributionDateTill) {
      const date = new Date(params.contributionDateTill);
      const y = date.getFullYear();
      const m = date.getMonth();

      const firstDay = new Date(y, m, 1);
      const lastDay = new Date(y, m + 1, 0);
      query.contributionDateTill = { $gte: firstDay, $lte: lastDay };
    }

    if (params.bdateFrom && !params.bdateTo) {
      query.bdate = { $gte: new Date(params.bdateFrom) };
    }

    if (!params.bdateFrom && params.bdateTo) {
      query.bdate = { $lte: new Date(params.bdateTo) };
    }

    if (params.addedBy) {
      if (params.addedBy !== 'external') {
        // eslint-disable-next-line no-param-reassign
        params.addedBy = mongoose.Types.ObjectId(params.addedBy);
      }

      query['addedBy.id'] = params.addedBy;
    }

    if (params.partyStatuses) {
      query.partyStatuses = { $in: params.partyStatuses };
    }

    if (params.publicStatuses) {
      query.publicStatuses = { $in: params.publicStatuses };
    }

    if (params.eventResults && params.eventResults.eventId) {
      let type;
      let status;

      switch (params.eventResults.type) {
        case 'sms':
          type = 'result.smsStatus';
          status = +params.eventResults.status ? 'delivered' : { $ne: 'delivered' };
          break;
        case 'call':
          type = 'result.callStatus';
          status = +params.eventResults.status ? 'finished' : { $ne: 'finished' };
          break;
        case 'tg':
          type = 'result.tgStatus';
          status = +params.eventResults.status ? 'delivered' : { $ne: 'delivered' };
          break;
        default:
      }

      const id = mongoose.Types.ObjectId(params.eventResults.eventId);

      query[type] = status;
      query['result.eventId'] = id;
    }

    if (user.role === 'district_admin') {
      query.districtOffice = user.access;
    }

    if (user.role === 'local_admin') {
      query.districtOffice = user.districtOffice;
      query.primaryOffice = user.primaryOffice;
    }

    if (user.role === 'status_admin') {
      query.$or = [
        { partyStatuses: { $in: [user.access] } },
        { publicStatuses: { $in: [user.access] } },
      ];
    }

    if (params.map) {
      query.$and = [
        { districtOffice: { $ne: 'Удаленные' } },
        { districtOffice: { $ne: 'Телеграм' } },
      ];
    }

    return (
      this
        .find(query)
        .sort({ createdAt: -1 })
        .exec()
    );
  },

  async updateSmsStatus(result) {
    return this.findOneAndUpdate(
      { mobilePhone: result.user, 'result.messageId': result.messageId },
      { 'result.$.smsStatus': result.smsStatus },
    );
  },

  async updateCallStatus(result) {
    return this.findOneAndUpdate(
      { mobilePhone: result.user, 'result.callId': result.callId },
      { 'result.$.callStatus': result.callStatus },
    );
  },

  async addResult(result, type) {
    let eventQuery;

    if (type === 'mobile') {
      eventQuery = { mobilePhone: result.user, 'result.eventId': result.eventId };
    }
    if (type === 'email') {
      eventQuery = { email: result.user, 'result.eventId': result.eventId };
    }
    if (type === 'telegram') {
      eventQuery = { telegramId: result.user, 'result.eventId': result.eventId };
    }

    const userHasEvent = await this.findOne(eventQuery, { 'result.$': 1 }).exec();

    if (userHasEvent) {
      const updatedResult = Object.assign(userHasEvent.result[0], result);
      return this.findOneAndUpdate(
        { _id: userHasEvent._id, 'result.eventId': result.eventId },
        { 'result.$': updatedResult },
      );
    }

    let userQuery;

    if (type === 'mobile') {
      userQuery = { mobilePhone: result.user };
    }
    if (type === 'email') {
      userQuery = { email: result.user };
    }
    if (type === 'telegram') {
      userQuery = { telegramId: result.user };
    }

    const user = await this.findOne(userQuery).exec();
    user.result.push(result);

    return user.save();
  },

  /**
   * Return new validation error
   * if error is a mongoose duplicate key error
   *
   * @param {Error} error
   * @returns {Error|APIError}
   */
  checkDuplicateEmail(error) {
    if (error.name === 'MongoError' && error.code === 11000) {
      let field = 'email';
      let message = 'Пользователь с такой почтой уже существует';
      if (error.message.includes('mobilePhone')) {
        field = 'mobilePhone';
        message = 'Пользователь с таким моб. телефоном уже существует';
      }
      return new APIError({
        message: 'Validation Error',
        errors: [
          {
            field,
            location: 'body',
            messages: [message],
          },
        ],
        status: httpStatus.CONFLICT,
        isPublic: true,
        stack: error.stack,
      });
    }
    return error;
  },

  // Statistic methods

  getMembersByAge(districtOffice) {
    const query = districtOffice
      ? { districtOffice }
      : {
        $and: [
          { districtOffice: { $ne: 'Удаленные' } },
          { districtOffice: { $ne: 'Телеграм' } },
        ],
      };

    return this.aggregate([
      {
        $facet: {
          membersByAge: [
            { $match: query },
            {
              $project: {
                _id: 0,
                age: {
                  $divide: [
                    {
                      $subtract: [
                        new Date(),
                        { $ifNull: ['$bdate', new Date()] },
                      ],
                    },
                    1000 * 86400 * 365,
                  ],
                },
              },
            },
            {
              $project: {
                range: {
                  $concat: [
                    { $cond: [{ $eq: ['$age', 0] }, 'null', ''] },
                    { $cond: [{ $lt: ['$age', 0] }, 'wrong', ''] },
                    { $cond: [{ $and: [{ $gt: ['$age', 0] }, { $lt: ['$age', 30] }] }, '30', ''] },
                    { $cond: [{ $and: [{ $gte: ['$age', 30] }, { $lt: ['$age', 40] }] }, '30_40', ''] },
                    { $cond: [{ $and: [{ $gte: ['$age', 40] }, { $lt: ['$age', 50] }] }, '40_50', ''] },
                    { $cond: [{ $and: [{ $gte: ['$age', 50] }, { $lt: ['$age', 60] }] }, '50_60', ''] },
                    { $cond: [{ $gte: ['$age', 60] }, 'greater60', ''] },
                  ],
                },
              },
            },
            {
              $group: {
                _id: '$range',
                count: {
                  $sum: 1,
                },
              },
            },
          ],
          averageAge: [
            { $match: query },
            {
              $project: {
                _id: 0,
                age: {
                  $floor: {
                    $divide: [
                      {
                        $subtract: [
                          new Date(),
                          { $ifNull: ['$bdate', new Date()] },
                        ],
                      },
                      1000 * 86400 * 365,
                    ],
                  },
                },
              },
            },
            { $match: { age: { $gt: 0 } } },
            { $group: { _id: 0, avgAge: { $avg: '$age' } } },
          ],
        },
      },
    ]).exec();
  },
};

/**
 * @typedef User
 */
module.exports = mongoose.model('User', userSchema);
