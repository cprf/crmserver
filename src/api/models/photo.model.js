const mongoose = require('mongoose');
const { TelegramClient } = require('messaging-api-telegram');
const User = require('./user.model');
const Rating = require('../models/rating.model');
const { botToken } = require('../../config/vars');

const client = TelegramClient.connect(botToken);

const STATUSES = {
  APPROVED: 1,
  NOTREVIEWED: 2,
};
/**
 * Task agit photo Schema
 * @private
 */
const photoSchema = new mongoose.Schema(
  {
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    task: { type: mongoose.Schema.Types.ObjectId, ref: 'Task' },
    status: {
      type: Number,
      enum: [STATUSES.APPROVED, STATUSES.NOTREVIEWED],
      default: STATUSES.NOTREVIEWED,
    },
    preview_path: String,
    original_path: String,
    gps: Object,
  },
  { timestamps: true },
);

const photoAcheivements = [
  {
    id: 'start',
    name: 'Начало положено. Расклейте 5 листовок.',
    points: 50,
  },
  {
    id: 'danger',
    name: 'Гроза Жилищника. Расклейте 50 листовок.',
    points: 100,
  },
  {
    id: 'experienced',
    name: 'Опытный расклейщик. Расклейте 500 листовок.',
    points: 500,
  },
];
// TODO: Добавлять бонусные баллы в таблицу рейтингов.
// Либо делать рейтинг на основе баллов из таблицы юзеров

photoSchema.post('save', async (doc, next) => {
  try {
    const photosCount = await Photo.count({
      user: doc.user,
      status: STATUSES.APPROVED,
    });

    const user = await User.findById(doc.user);
    const bonus = { user_id: doc.user, type: 'bonus' };

    if (photosCount === 5) {
      await User.update(
        { _id: doc.user },
        {
          $addToSet: { achievements: photoAcheivements[0] },
          $inc: { points: photoAcheivements[0].points },
        },
      );

      bonus.points = photoAcheivements[0].points;

      const bonusRating = new Rating(bonus);
      await bonusRating.save();

      const message = `Ура! Вами получено достижение: ${photoAcheivements[0].name}`;
      await client.sendMessage(user.telegramId, message);
    } else if (photosCount === 50) {
      await User.update(
        { _id: doc.user },
        {
          $addToSet: { achievements: photoAcheivements[1] },
          $inc: { points: photoAcheivements[1].points },
        },
      );

      bonus.points = photoAcheivements[1].points;

      const bonusRating = new Rating(bonus);
      await bonusRating.save();

      const message = `Ура! Вами получено достижение: ${photoAcheivements[1].name}`;
      await client.sendMessage(user.telegramId, message);
    } else if (photosCount === 500) {
      await User.update(
        { _id: doc.user },
        {
          $addToSet: { achievements: photoAcheivements[2] },
          $inc: { points: photoAcheivements[2].points },
        },
      );

      bonus.points = photoAcheivements[2].points;

      const bonusRating = new Rating(bonus);
      await bonusRating.save();

      const message = `Ура! Вами получено достижение: ${photoAcheivements[2].name}`;
      await client.sendMessage(user.telegramId, message);
    }

    return next();
  } catch (error) {
    return next(error);
  }
});

photoSchema.statics = {
  list(params) {
    const query = params.map ? { gps: { $exists: true } } : { status: STATUSES.NOTREVIEWED };

    return this.find(query)
      .populate({ path: 'user', select: 'fio' })
      .populate({ path: 'task', select: 'name' })
      .lean()
      .exec();
  },
  async get(id) {
    try {
      let photo;

      if (mongoose.Types.ObjectId.isValid(id)) {
        photo = await this.findById(id).exec();
      }
      return photo;
    } catch (error) {
      throw error;
    }
  },
  async remove(id) {
    try {
      const photo = await this.findByIdAndRemove(id).exec();
      return photo;
    } catch (error) {
      throw error;
    }
  },
};

/**
 * @typedef Photo
 */
const Photo = mongoose.model('Photo', photoSchema);
module.exports = Photo;
