const mongoose = require('mongoose');

/**
* User Status types
*/
const types = ['public', 'party'];

/**
 * User status Schema
 * @private
 */
const statusSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    index: true,
  },
  type: {
    type: String,
    required: true,
    index: true,
  },
});

statusSchema.statics = {
  types,

  list(query) {
    return this.find(query).exec();
  },
};

/**
 * @typedef PartyStatus
 */
const UserStatus = mongoose.model('UserStatus', statusSchema);
module.exports = UserStatus;
