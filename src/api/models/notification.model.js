const mongoose = require('mongoose');
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');

/**
 * Notifications Schema
 * @private
 */
const NotificationSchema = new mongoose.Schema({
  districtOffice: String,
  email: String,
  phone: String,
  fio: String,
});

NotificationSchema.statics = {
  list() {
    return this.find().exec();
  },
  findByOffice(params) {
    return this.findOne(params).exec();
  },
  async get(id) {
    try {
      let record;

      if (mongoose.Types.ObjectId.isValid(id)) {
        record = await this.findById(id).exec();
      }
      if (record) {
        return record;
      }

      throw new APIError({
        message: 'Record does not exist',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },
};

/**
 * @typedef Notifications
 */
const Notification = mongoose.model('Notification', NotificationSchema);
module.exports = Notification;
