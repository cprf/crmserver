const mongoose = require('mongoose');
const mailService = require('../services/mail.service');
const APIError = require('../utils/APIError');
const httpStatus = require('http-status');
/**
 * Event Schema
 * @private
 */
const eventSchema = new mongoose.Schema({
  type: Object,
  name: String,
  isTask: Boolean,
  organizer: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  date: Date,
  notifyDate: Date,
  description: String,
  emailNotification: {
    date: Date,
    text: String,
    subject: String,
    recipients: Array,
  },
  mobileNotification: {
    date: Date,
    record: Object,
    recipients: Array,
  },
  smsNotification: {
    date: Date,
    text: String,
    recipients: Array,
  },
  telegramNotification: {
    date: Date,
    text: String,
    recipients: Array,
    smsRecipients: Array,
  },
  nearestAddress: String,
  location: {
    coordinates: Array,
    type: {
      type: String,
      default: 'Point',
    },
  },
  result: Object,
}, {
  timestamps: true,
});

eventSchema.pre('save', function save(next) {
  if (!this.location.coordinates.length) {
    this.location.coordinates = undefined;
    this.location.type = undefined;
  }
  next();
});

eventSchema.statics = {
  list() {
    return this.find().populate({ path: 'organizer', select: 'role districtOffice access' }).lean().exec();
  },
  /**
   * Get event
   *
   * @param {ObjectId} id - The objectId of event.
   * @returns {Promise<event, APIError>}
   */
  async get(id) {
    try {
      let event;

      if (mongoose.Types.ObjectId.isValid(id)) {
        event = await this.findById(id).populate({ path: 'organizer', select: 'role fio access' }).lean().exec();
      }
      if (event) {
        return event;
      }

      throw new APIError({
        message: 'Event does not exist',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },
  async remove(id) {
    try {
      const event = await this.findByIdAndRemove(id).exec();
      mailService.removePlannedNotifications(id);
      return event;
    } catch (error) {
      throw error;
    }
  },
};

/**
 * @typedef Event
 */
const Event = mongoose.model('Event', eventSchema);
module.exports = Event;
