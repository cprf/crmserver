const mongoose = require('mongoose');
const User = require('../models/user.model');
const Rating = require('../models/rating.model');
const { botToken } = require('../../config/vars');
const { TelegramClient } = require('messaging-api-telegram');

const client = TelegramClient.connect(botToken);

const STATUSES = {
  NOTREVIEWED: 0,
  APPROVED: 1,
  REMOVED: 2,
};

/**
 * Creative task results Schema
 * @private
 */
const creativeSchema = new mongoose.Schema(
  {
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    task: { type: mongoose.Schema.Types.ObjectId, ref: 'Task' },
    link: String,
    status: {
      type: Number,
      enum: [STATUSES.APPROVED, STATUSES.NOTREVIEWED, STATUSES.REMOVED],
      default: STATUSES.NOTREVIEWED,
    },
  },
  { timestamps: true },
);

const creativeAchievement = {
  id: 'creative',
  name: 'Творческий. Выполните 5 творческих заданий.',
  points: 300,
};

creativeSchema.post('save', async (doc, next) => {
  try {
    const taskCount = await Creative.count({
      user: doc.user,
      status: STATUSES.APPROVED,
    });

    const user = await User.findById(doc.user);

    if (taskCount === 5) {
      await User.update(
        { _id: doc.user },
        {
          $addToSet: { achievements: creativeAchievement },
          $inc: { points: creativeAchievement.points },
        },
      );

      const bonus = {
        user_id: doc.user,
        points: creativeAchievement.points,
        type: 'bonus',
      };

      const bonusRating = new Rating(bonus);
      await bonusRating.save();

      const message = `Ура! Вами получено достижение: ${creativeAchievement.name}`;
      await client.sendMessage(user.telegramId, message);
    }

    return next();
  } catch (error) {
    return next(error);
  }
});

creativeSchema.statics = {
  STATUSES: {
    NOTREVIEWED: 0,
    APPROVED: 1,
    REMOVED: 2,
  },

  listByTask(task) {
    return this.find({ task, status: this.STATUSES.NOTREVIEWED })
      .populate({ path: 'user', select: 'fio' })
      .lean()
      .exec();
  },
};

/**
 * @typedef Creative
 */
const Creative = mongoose.model('Creative', creativeSchema);
module.exports = Creative;
