const mongoose = require('mongoose');

/**
 * User history Schema
 * @private
 */
const userHistorySchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    index: true,
  },
  fields: Array,
  updatedBy: String,
  updatedAt: Date,
});

userHistorySchema.statics = {
  get(id) {
    return this.find({ userId: id }).exec();
  },
};

/**
 * @typedef UserHistory
 */
const UserHistory = mongoose.model('UserHistory', userHistorySchema);
module.exports = UserHistory;

