const axios = require('axios');
const { botToken } = require('../../config/vars');
const { MTProto } = require('telegram-mtproto');

const api = {
  invokeWithLayer: 0xda9b0d0d,
  layer: 57,
  initConnection: 0x69796de9,
  api_id: 49631,
  app_version: '1.0.1',
  lang_code: 'en',
};

const server = { webogram: true, dev: false };
const client = MTProto({ server, api });

const sendMessageUrl = `https://api.telegram.org/bot${botToken}/sendMessage`;
const sendActionUrl = `https://api.telegram.org/bot${botToken}/sendChatAction`;

exports.checkPhone = phone => client('auth.checkPhone', { phone_number: phone });
exports.sendMessage = (chatId, text) => axios.get(sendMessageUrl, {
  params: { chat_id: chatId, text },
});

// Метод для проверки кто заблокировал бота
exports.sendAction = chatId => axios.get(sendActionUrl, {
  params: { chat_id: chatId, action: 'typing' },
  validateStatus: status => status >= 200 && status < 500,
});
