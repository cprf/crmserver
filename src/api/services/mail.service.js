/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
const nodemailer = require('nodemailer');
const schedule = require('node-schedule');
const axios = require('axios');
const User = require('../models/user.model');
const tgService = require('./telegram.service');
const { baseUrl } = require('../../config/vars');

const accountSid = 'AC61a91cbe7b62d3017c40fdae18f44802';
const authToken = 'e39536f2522109dd06f0827169b1c67e';
const client = require('twilio')(accountSid, authToken);

const options = {
  service: 'gmail',
  auth: {
    user: 'kprfmessage@gmail.com',
    pass: 'kprfmessage12345',
  },
};

const transporter = nodemailer.createTransport(options);

const notifyByEmail = (event) => {
  const message = {
    from: options.auth.user,
    subject: event.emailNotification.subject,
    priority: 'high',
    text: event.emailNotification.text,
    attachments: [],
  };
  if (event.emailNotification.attachments.length) {
    event.emailNotification.attachments.forEach((attachment) => {
      message.attachments.push(attachment);
    });
  }

  const jobName = `${event._id}_email`;
  const jobDate = new Date(event.emailNotification.date);

  schedule.scheduleJob(jobName, jobDate, async () => {
    try {
      // eslint-disable-next-line no-restricted-syntax
      for (const email of event.emailNotification.recipients) {
        // eslint-disable-next-line no-await-in-loop
        await delay(3000);
        message.to = email;
        transporter.sendMail(message, (err) => {
          if (err) {
            console.log('error in email sending', err);
          } else {
            const result = {
              eventId: event._id,
              name: event.name,
              date: new Date(event.date),
              emailStatus: 'sent',
              user: email,
            };
            User.updateResult(result, 'email');
          }
        });
      }
    } catch (error) {
      console.error('Error while sending messages', error);
    }
  });
};

const makeResultObject = (event, message, recipient) => ({
  eventId: event._id,
  name: event.name,
  date: new Date(event.date),
  smsStatus: message.status,
  messageId: message.sid,
  user: recipient,
});

const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

const notifyBySMS = (event, withTelegram) => {
  const body = withTelegram
    ? event.telegramNotification.text
    : event.smsNotification.text;

  const date = withTelegram
    ? event.telegramNotification.date
    : event.smsNotification.date;

  const recipients = withTelegram
    ? event.telegramNotification.smsRecipients
    : event.smsNotification.recipients;

  const smsOptions = {
    from: '+19168317134',
    body,
    statusCallback: `${baseUrl}/v1/events/smsstatus`,
  };

  const jobName = `${event._id}_sms`;
  const jobDate = new Date(date);

  const sendSms = (recipient) => {
    smsOptions.to = `+${recipient}`;
    return client.messages.create(smsOptions);
  };

  schedule.scheduleJob(jobName, jobDate, async () => {
    const totalMessagesSent = [];
    console.log('Total recipients: ', recipients.length);

    // eslint-disable-next-line no-restricted-syntax
    for (const recipient of recipients) {
      try {
        await delay(5000);
        const message = await sendSms(recipient);
        const result = makeResultObject(event, message, recipient);
        const user = await User.addResult(result, 'mobile');

        totalMessagesSent.push(user._id);
      } catch (error) {
        console.error(`MailService::notifyBySMS at ${new Date()} `, error);
      }
    }

    console.log('Total messages sent: ', totalMessagesSent.length);
  });
};

const notifyByCall = (event) => {
  console.log('Notify by Call:event', event);
  const { date, record, recipients } = event.mobileNotification;
  const jobName = `${event._id}_phone`;
  const jobDate = new Date(date);
  const url = 'https://lk.calldog.ru/apiCalls/create';

  const params = {
    apiKey: 'Trx2Edw08xI44KlbNAaLzyM6Vpci3wiGMxPGI5BYtaVBAlD46MujjiVKMsAu',
    outgoingPhone: '79096714615',
    webhookUrl: `${baseUrl}/v1/events/callstatus`,
    record: {
      id: record.id,
      gender: record.gender,
    },
  };

  const createCall = phone =>
    new Promise((resolve, reject) => {
      setTimeout(() => {
        params.phone = phone;
        axios
          .post(url, params)
          .then(async (response) => {
            const call = response.data.data[0];
            const result = {
              eventId: event._id,
              name: event.name,
              date: new Date(event.date),
              callId: call.id,
              callStatus: 'created',
              user: phone,
            };
            await User.addResult(result, 'mobile');
            resolve();
          })
          .catch((error) => {
            console.log('error', error.response);
            reject(error);
          });
      }, 15000);
    });

  schedule.scheduleJob(jobName, jobDate, async () => {
    console.log('Notify by Call:scheduling', jobDate);
    for (const phone of recipients) {
      await createCall(phone);
    }
  });
};

exports.verify = () => {
  transporter.verify((error) => {
    if (error) {
      console.log(error);
    } else {
      console.log('Server is ready to take our messages');
    }
  });
};

const notifyByTelegram = (event, user) => {
  const {
    date,
    text,
    recipients,
    smsRecipients,
  } = event.telegramNotification;

  const jobName = `${event._id}_tg`;
  const jobDate = new Date(date);

  const tgText = `Мероприятие: ${event.name}.\nОрганизатор: ${user.fio}.\n\n${text}`;

  schedule.scheduleJob(jobName, jobDate, async () => {
    // eslint-disable-next-line no-restricted-syntax
    for (const recipient of recipients) {
      try {
        await delay(5000);
        const message = await tgService.sendMessage(recipient, tgText);

        if (message.data.ok) {
          const result = {
            eventId: event._id,
            name: event.name,
            date: new Date(event.date),
            tgStatus: 'delivered',
            user: recipient,
          };

          await User.addResult(result, 'telegram');
        }
      } catch (error) {
        console.error(`TelegramService::notifyByTelegram at ${new Date()} `, error);
      }
    }
  });

  if (smsRecipients.length) {
    notifyBySMS(event, true);
  }
};

exports.notify = (event, user) => {
  if (event.emailNotification.date) {
    notifyByEmail(event);
  }
  if (event.smsNotification.date) {
    notifyBySMS(event);
  }
  if (event.mobileNotification.date) {
    notifyByCall(event);
  }
  if (event.telegramNotification.date) {
    notifyByTelegram(event, user);
  }
};

exports.nofityExternal = (params) => {
  const message = {
    from: options.auth.user,
    subject: 'Новая заявка',
    priority: 'high',
    text: params.emailText,
    to: params.email,
  };

  const sms = {
    from: '+19168317134',
    body: params.smsText,
    to: params.phone,
  };

  client.messages.create(sms).then((m) => {
    console.log('sms sent', m.status);
  });

  transporter.sendMail(message, (err, info) => {
    if (err) {
      console.log('error in email sending', err);
    } else {
      console.log('email info', info);
    }
  });
};

exports.removePlannedNotifications = (eventId) => {
  const smsJobName = `${eventId}_sms`;
  const emailJobName = `${eventId}_email`;
  const phoneJobName = `${eventId}_phone`;
  const tgJobName = `${eventId}_tg`;

  const smsJob = schedule.scheduledJobs[smsJobName];
  const emailJob = schedule.scheduledJobs[emailJobName];
  const phoneJob = schedule.scheduledJobs[phoneJobName];
  const tgJob = schedule.scheduledJobs[tgJobName];

  if (smsJob) smsJob.cancel();
  if (emailJob) emailJob.cancel();
  if (phoneJob) phoneJob.cancel();
  if (tgJob) tgJob.cancel();
};
