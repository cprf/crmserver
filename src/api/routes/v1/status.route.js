const express = require('express');
const validate = require('express-validation');
const controller = require('../../controllers/status.controller');
const { authorize } = require('../../middlewares/auth');
const {
  createStatus,
} = require('../../validations/status.validation');

const router = express.Router();

router
  // v1/statuses
  .route('/')
  .get(authorize(), controller.list)
  .post(authorize(), validate(createStatus), controller.create);

module.exports = router;
