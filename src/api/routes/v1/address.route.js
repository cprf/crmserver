const express = require('express');
const controller = require('../../controllers/address.controller');

const router = express.Router();

router
  // v1/addresses
  .route('/')
  .get(controller.query);

module.exports = router;
