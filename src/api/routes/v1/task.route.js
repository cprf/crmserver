const express = require('express');
const multer = require('multer');
const controller = require('../../controllers/task.controller');
const { authorize } = require('../../middlewares/auth');

const router = express.Router();
const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './uploads/tasks');
  },
  filename(req, file, cb) {
    let ext = 'jpg';
    if (file.mimetype === 'image/png') {
      ext = 'png';
    }
    cb(null, `${Date.now()}.${ext}`);
  },
});

const upload = multer({ storage });

router
  // v1/tasks
  .route('/')
  .get(authorize(), controller.list)
  .post(authorize(), upload.single('taskpic'), controller.create);

router
  // v1/tasks/photos
  .route('/photos')
  .get(authorize(), controller.getPhotos);

router
  // v1/tasks/photos/:id
  .route('/photos/:id')
  .put(authorize(), controller.updatePhoto)
  .delete(authorize(), controller.removePhoto);

router
  .route('/:taskId')
  .get(authorize(), controller.get)
  .delete(authorize(), controller.remove);

router.route('/results/:id').patch(authorize(), controller.updateResultStatus);

module.exports = router;
