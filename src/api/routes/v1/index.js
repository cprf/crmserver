const express = require('express');
const userRoutes = require('./user.route');
const authRoutes = require('./auth.route');
const statusRoutes = require('./status.route');
const eventRoutes = require('./events.route');
const addressRoutes = require('./address.route');
const taskRoutes = require('./task.route');
const ratingRoutes = require('./rating.route');
const notificationsRoutes = require('./notifications.route');
const ratingController = require('../../controllers/rating.controller');

const router = express.Router();

/**
 * GET v1/handleLink
 * Save info about clicked link
 */
router.get('/handleLink', ratingController.handleLink);

/**
 * GET v1/docs
 */
router.use('/docs', express.static('docs'));

router.use('/users', userRoutes);
router.use('/auth', authRoutes);
router.use('/statuses', statusRoutes);
router.use('/events', eventRoutes);
router.use('/addresses', addressRoutes);
router.use('/tasks', taskRoutes);
router.use('/rating', ratingRoutes);
router.use('/notifications', notificationsRoutes);

module.exports = router;
