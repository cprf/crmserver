const express = require('express');
const controller = require('../../controllers/event.controller');
const { authorize } = require('../../middlewares/auth');

const router = express.Router();

router
  // v1/events
  .route('/')
  .get(authorize(), controller.list)
  .post(authorize(), controller.create);

// POST v1/events/smsstatus
router.post('/smsstatus', controller.smsStatus);
// POST v1/events/callstatus
router.post('/callstatus', controller.callStatus);

router
  .route('/:eventId')
  .get(authorize(), controller.get)
  .delete(authorize(), controller.remove);

module.exports = router;
