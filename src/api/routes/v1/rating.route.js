const express = require('express');
const controller = require('../../controllers/rating.controller');
const { authorize } = require('../../middlewares/auth');

const router = express.Router();

router
  // v1/rating
  .route('/')
  .get(authorize(), controller.getRating);

module.exports = router;
