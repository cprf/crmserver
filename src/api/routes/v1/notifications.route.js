const express = require('express');
const controller = require('../../controllers/notifications.controller');
const { authorize } = require('../../middlewares/auth');

const router = express.Router();

router
  // v1/rating
  .route('/')
  .get(authorize(), controller.list)
  .post(authorize(), controller.create)
  .patch(authorize(), controller.update);

module.exports = router;
