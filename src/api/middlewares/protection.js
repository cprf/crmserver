const { omit } = require('lodash');

const excludeFieldsFromUpdate = ['result', 'points', 'level', 'achievements'];

exports.protectCreation = () => (req, res, next) => {
  if (req.body.role && req.body.role !== 'user') {
    req.body = omit(req.body, ['role']);
  }
  next();
};

exports.protectUpdate = () => (req, res, next) => {
  req.body = omit(req.body, excludeFieldsFromUpdate);

  if (req.body.role && req.body.role === 'admin') {
    req.body = omit(req.body, ['role']);
  } else if (req.user.role === 'user' || req.user.role === 'status_admin') {
    req.body = omit(req.body, ['role']);
  } else if (req.user.role === 'local_admin' && req.body.role !== 'user') {
    req.body = omit(req.body, ['role']);
  } else if (
    req.user.role === 'district_admin' &&
    (req.body.role !== 'local_admin' && req.body.role !== 'user')
  ) {
    req.body = omit(req.body, ['role']);
  } else if (
    req.user.role === 'city_admin' &&
    (req.body.role === 'admin' || req.body.role === 'city_admin')
  ) {
    req.body = omit(req.body, ['role']);
  }

  next();
};
