const path = require('path');

// import .env variables
require('dotenv-safe').load({
  path: path.join(__dirname, `../../.env.${process.env.CITY}`),
  sample: path.join(__dirname, '../../.env.example'),
});

module.exports = {
  city: process.env.CITY_RUS,
  baseUrl: process.env.BASE_URL,
  dbName: process.env.DB_NAME,
  botToken: process.env.BOT_TOKEN,
  env: process.env.NODE_ENV,
  port: process.env.PORT,
  jwtSecret: process.env.JWT_SECRET,
  jwtExpirationInterval: process.env.JWT_EXPIRATION_MINUTES,
  mongo: {
    uri: process.env.NODE_ENV === 'test' ? process.env.MONGO_URI_TESTS : process.env.MONGO_URI,
  },
  logs: process.env.NODE_ENV === 'production' ? 'combined' : 'dev',
};
